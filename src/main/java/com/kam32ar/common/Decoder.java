package com.kam32ar.common;

import java.util.List;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;

public class Decoder extends ReplayingDecoder<DecodingState> {

	private Envelope envelope;

	public Decoder() {
		reset();
	}

	public void reset() {
		checkpoint(DecodingState.TYPE);
		envelope = new Envelope();
	}

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
		switch (state()) {
		case TYPE:
			envelope.setType(Type.fromByte(in.readByte()));
			checkpoint(DecodingState.PAYLOAD_LENGTH);
			break;
		case PAYLOAD_LENGTH:
			int size = in.readInt();
			if (size <= 0)
				throw new Exception("Invalid content size");
			byte[] content = new byte[size];
			envelope.setPayload(content);
			checkpoint(DecodingState.PAYLOAD);
			break;
		case PAYLOAD:
			in.readBytes(envelope.getPayload(), 0, envelope.getPayload().length);
			checkpoint(DecodingState.PAYLOAD);
			try {
				out.add(envelope);
			} finally {
				this.reset();
			}
			break;
		default:
			throw new Exception("Unknown decoding state: " + state());
		}
	}

}
