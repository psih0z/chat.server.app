package com.kam32ar.common;

public enum RequestType {

	HANDSHAKE((byte) 0x01), UNKNOWN((byte) 0x00);

	private final byte b;

	private RequestType(byte b) {
		this.b = b;
	}

	public static RequestType fromByte(byte b) {
		for (RequestType code : values()) {
			if (code.b == b) {
				return code;
			}
		}

		return UNKNOWN;
	}

	public byte getByteValue() {
		return b;
	}

}
