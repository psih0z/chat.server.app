package com.kam32ar.common;

import java.nio.charset.Charset;

public class Envelope {

	private Type type;
	private byte[] payload;

	public Envelope() {
	}

	public Envelope(Type type) {
		this.type = type;
		this.payload = "".getBytes();
	}

	public Envelope(Type type, byte[] payload) {
		this.type = type;
		this.payload = payload;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public byte[] getPayload() {
		return payload;
	}

	public void setPayload(byte[] payload) {
		this.payload = payload;
	}

	public void setPayloadFromString(String payload) {
		if (payload != null) {
			this.payload = payload.getBytes(Charset.forName("UTF-8"));
		}
	}

	// low level overrides
	// --------------------------------------------------------------------------------------------

	@Override
	public String toString() {
		return new StringBuilder().append("Envelope{").append("type=").append(type).append(", payload=")
				.append(payload == null ? null : payload.length + "bytes").append('}').toString();
	}

}
